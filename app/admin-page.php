<?php defined('ABSPATH') or die(-1);

use Illuminate\Support\Carbon;

add_action('admin_menu', function() {
	add_submenu_page('woocommerce', WASB_AUTOBILLING_PAGE_TITLE_TEXT, WASB_AUTOBILLING_PAGE_TITLE_TEXT,
		'administrator', 'autobilling-settings', function() {

			//GET CRON TIMESTAMP
			$timestamp = wp_next_scheduled( WASB_AUTOBILLING_EVENT , wasb_autobilling_event_args());
			$formattedCronScheduled = date('Y-m-d H:i:s (l)', $timestamp);

			//GET WASB_PRICING_ROLE_OPTION_NAME
			$pricing_role = get_blog_option( get_current_blog_id(), WASB_PRICING_ROLE_OPTION_NAME, 'customer' );


			//GET ALLOWED ROLES
			$wc_rbp_general = get_blog_option( get_current_blog_id(), 'wc_rbp_general', false );

			$allowed_roles = false;

			if( $wc_rbp_general ) {
				$allowed_roles = @$wc_rbp_general['wc_rbp_allowed_roles'];
			}

			//GET KIOSK MANAGER
			$kiosk_manager_ID = get_blog_option( get_current_blog_id(), WASB_KIOSK_MANAGER_OPTION_NAME, false );

			$args = array(
				'blog_id'   => get_current_blog_id(),
				'role'      => 'kiosk_manager',
                'fields'    => 'all_with_meta'
			);

			$kiosk_managers = get_users( $args );

			//GET BILLING DATE RANGE
			$autobilling_from_date = get_blog_option( get_current_blog_id(), WASB_ONDEMAND_FROM_DATE, wasb_default_date_range() );
			$autobilling_to_date = get_blog_option( get_current_blog_id(), WASB_ONDEMAND_TO_DATE, wasb_default_date_range('to') );

			//GET CLIENT INFO
            $sliced_client_business = esc_html( get_user_meta($kiosk_manager_ID, '_sliced_client_business', true) );
            $sliced_client_address = esc_html( get_user_meta($kiosk_manager_ID, '_sliced_client_address', true) );
            $sliced_client_extra_info = esc_html( get_user_meta($kiosk_manager_ID, '_sliced_client_extra_info', true) );

            //GET KIOSK INFO
			$sliced_business = get_blog_option( get_current_blog_id(), 'sliced_business', false );

			//GET QUOTE SETTINGS
			$sliced_quote = get_blog_option( get_current_blog_id(), 'sliced_quotes', false );

			// GET SUMMARY
			$date_from = wasb_default_date_range();
			$date_to = wasb_default_date_range('to');

			$sale_summary = wasb_site_sales_summary_by_date_range($date_from, $date_to, $pricing_role);


            include WASB_DIR . '/app/admin-page.tpl.php';
		});
});