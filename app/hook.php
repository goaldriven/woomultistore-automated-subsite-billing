<?php

use Illuminate\Support\Carbon;

defined('ABSPATH') or die(-1);

// DISABLE TEMPORARILY: stock transfer handled by plugin 'Woomultistore Stock Transfer Billing'
//add_action(  'added_term_relationship', 'wasb_sliced_invoice_transition_post_status', 10, 3 );

/**
 * @param $invoice
 * @param $status
 */
function wasb_sliced_invoice_transition_post_status( $invoiceID, $statusTermId, $statusTaxonomy ) {
	if( $statusTaxonomy != 'invoice_status' ) return;

	$currentStatus = get_term($statusTermId, $statusTaxonomy);

	wasb_logger( "wasb_sliced_invoice_transition_post_status:START: {$invoiceID}, {$currentStatus->slug}" );

	$stock_transfer = get_post_meta( $invoiceID, WASB_STOCK_TRANSFER_META, true );
	$last_stock_transfer_time = get_post_meta( $invoiceID, WASB_LAST_STOCK_TRANSFER_TIME_META, true );
	$main_site_id = 1;
	$current_site = get_current_blog_id();

	// If the invoice just paid (never process stock transfer before)
	if ( $currentStatus->slug == 'paid' && !$last_stock_transfer_time ) {

		// Process stock transfer
		collect($stock_transfer)->each(function( $item ) use ( $invoiceID, $main_site_id, $current_site ) {
			// Skip if no product SKU
			if( empty($item['product_SKU']) ) return;

			// STOCK TRANSFER
			// 1) Deduct mainsite stock
			// 2) Topup subsite stock

			switch_to_blog( $main_site_id );

			$product_id = wc_get_product_id_by_sku( $item['product_SKU'] );

			// Skip if product not found
			if( $product_id < 1 ) return;

			$stock = get_post_meta( $product_id, '_stock', true);
			$new_stock = intval( $stock ) - $item['qty'];

			// 1)
			$success1 = update_post_meta( $product_id, '_stock', $new_stock );
			wasb_logger( "stock-transfer:success1: {$success1}" );

			if( $success1 ) {

				switch_to_blog( $current_site );

				$stock = get_post_meta( $item['product_ID'], '_stock', true);
				$new_stock = intval( $stock ) + $item['qty'];

				// 2)
				if( $success2 = update_post_meta( $item['product_ID'], '_stock', $new_stock) ) {
					wasb_logger( "stock-transfer:success2: {$success2}" );

					// Record stock transfer
					add_post_meta( $invoiceID, WASB_STOCK_TRANSFER_RECORDS_META, [
						'product_ID' => $item['product_ID'],
						'qty' => $item['qty'],
						'time' => Carbon::now()->format('Y-m-d H:i:s')
					]);

					// Mark last stock transfered
					update_post_meta( $invoiceID, WASB_LAST_STOCK_TRANSFER_TIME_META, true );
				}
			}
		});

	} else if ( $currentStatus->slug != 'paid' && $last_stock_transfer_time ) {

		// Process stock return
		collect($stock_transfer)->each(function( $item ) use ( $invoiceID, $main_site_id, $current_site ) {
			// Skip if no product SKU
			if( empty($item['product_SKU']) ) return;

			// STOCK RETURN
			// 1) Topup mainsite stock
			// 2) Deduct subsite stock

			switch_to_blog( $main_site_id );

			$product_id = wc_get_product_id_by_sku( $item['product_SKU'] );

			// Skip if product not found
			if( $product_id < 1 ) return;

			$stock = get_post_meta( $product_id, '_stock', true);
			$new_stock = intval( $stock ) + $item['qty'];

			// 1)
			$success1 = update_post_meta( $product_id, '_stock', $new_stock );
			wasb_logger( "stock-return:success1: {$success1}" );

			switch_to_blog( $current_site );

			if( $success1 ) {

				switch_to_blog( $current_site );

				$stock = get_post_meta( $item['product_ID'], '_stock', true);
				$new_stock = intval( $stock ) - $item['qty'];

				// 2)
				if( $success2 = update_post_meta( $item['product_ID'], '_stock', $new_stock) ) {
					wasb_logger( "stock-return:success2: {$success1}" );

					// Delete stock transfer record
					delete_post_meta( $invoiceID, WASB_STOCK_TRANSFER_RECORDS_META );

					// Delete last stock transfered mark
					delete_post_meta( $invoiceID, WASB_LAST_STOCK_TRANSFER_TIME_META );
				}
			}
		});

	}

	wasb_logger( "wasb_sliced_invoice_transition_post_status:END: {$invoiceID}, {$currentStatus->slug}" );
}