<?php defined( 'ABSPATH' ) or die( - 1 );

use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;

if ( ! function_exists( 'wasb_default_date_range' ) ) {
	/**
	 * AUTOBILLING date range default
	 *
	 * @param string $type from | to
	 *
	 * @return string
	 */
	function wasb_default_date_range( $type = 'from' ) {
		switch ( $type ) {
			case 'to':
				return Carbon::today()->endOfWeek( Carbon::SUNDAY )->format( 'Y-m-d' );
			default:
				return Carbon::today()->startOfWeek()->format( 'Y-m-d' );
				break;
		}
	}
}

if ( ! function_exists( 'wasb_autobilling_event_reschedule' ) ) {
	/**
	 * CRON WASB_AUTOBILLING_EVENT ARGUMENTS
	 *
	 * @return array
	 */
	function wasb_autobilling_event_reschedule( $day = null ) {
		wp_clear_scheduled_hook( WASB_AUTOBILLING_EVENT, wasb_autobilling_event_args() );

		if ( ! $day ) {
			$day = Carbon::SUNDAY;
		}

		wp_schedule_event( Carbon::today()->endOfWeek( $day )->endOfDay()->minute( 50 )->format( 'U' ),
			WASB_AUTOBILLING_EVENT_RECURRENCE, WASB_AUTOBILLING_EVENT, wasb_autobilling_event_args() );
	}
}


if ( ! function_exists( 'wasb_autobilling_event_args' ) ) {
	/**
	 * CRON WASB_AUTOBILLING_EVENT ARGUMENTS
	 *
	 * @return array
	 */
	function wasb_autobilling_event_args() {
		return [ get_current_blog_id() ];
	}
}


if ( ! function_exists( 'wasb_get_product_sold_by_date_range' ) ) {
	/**
	 * @param $date_from
	 * @param $date_to
	 *
	 * @return mixed|integer|null
	 */
	function wasb_get_product_sold_by_date_range( $date_from, $date_to ) {
		global $wpdb;

		return $wpdb->get_results(
			wasb_get_product_sold_by_date_range_sql( $date_from, $date_to )
		);
	}
}

if ( ! function_exists( 'wasb_get_product_sold_by_date_range_sql' ) ) {
	function wasb_get_product_sold_by_date_range_sql( $date_from, $date_to ) {
		global $wpdb;

		$sql = "
	    SELECT order_meta.meta_value as product_id, COUNT(order_meta.meta_value) AS sale_count
	    FROM {$wpdb->prefix}woocommerce_order_items AS order_items
	    INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS order_meta ON order_items.order_item_id = order_meta.order_item_id
	    INNER JOIN {$wpdb->posts} AS posts ON order_meta.meta_value = posts.ID
	    WHERE order_items.order_item_type = 'line_item'
	    AND order_meta.meta_key = '_product_id'
	    AND order_items.order_id IN (
	        SELECT posts.ID AS post_id
	        FROM {$wpdb->posts} AS posts
	        WHERE posts.post_type = 'shop_order'
	            AND posts.post_status IN ('wc-completed')
	            AND DATE(posts.post_date) BETWEEN %s AND %s
	    )
	    GROUP BY order_meta.meta_value";

		return $wpdb->prepare( $sql, $date_from, $date_to );
	}
}


if ( ! function_exists( 'wasb_site_sales_summary_by_date_range' ) ) {
	/**
	 * @param $date_from
	 * @param $date_to
	 * @param $role_name
	 * @param string $price_key
	 *
	 * @return Collection
	 */
	function wasb_site_sales_summary_by_date_range( $date_from, $date_to, $role_name, $price_key = 'selling_price' ) {
		$products_sold = wasb_get_product_sold_by_date_range( $date_from, $date_to );

		return collect( $products_sold )->map( function ( $product_sold ) use ( $role_name, $price_key ) {

			list( $unit_price, $row_total ) = wasb_line_item_total( $product_sold->product_id, $product_sold->sale_count, $role_name, $price_key );

			$product_sold->unitPrice          = $unit_price;
			$product_sold->rowTotal           = $row_total;
			$product_sold->formattedUnitPrice = wc_price( $unit_price );
			$product_sold->formattedRowTotal  = wc_price( $row_total );
			$product_sold->sku                = get_post_meta( $product_sold->product_id, '_sku', true );

			return $product_sold;
		} );
	}
}

if ( ! function_exists( 'wasb_line_item_total' ) ) {
	function wasb_line_item_total( $product_id, $qty, $role_name, $price_key ) {
		$unit_price = wasb_product_price_by_role( $product_id, $role_name, $price_key );
		$row_total  = $unit_price * $qty;

		return [
			$unit_price,
			$row_total
		];
	}
}

if ( ! function_exists( 'wasb_get_current_role' ) ) {
	function wasb_get_current_role() {
		return get_blog_option( get_current_blog_id(), WASB_PRICING_ROLE_OPTION_NAME, 'customer' );
	}
}


if ( ! function_exists( 'wasb_product_price_by_role' ) ) {
	/**
	 * @param $product_id
	 * @param $role_name
	 * @param string $price_key
	 *
	 * @return mixed|string
	 */
	function wasb_product_price_by_role( $product_id, $role_name, $price_key = 'selling_price' ) {
		$pf = new WC_Product_Factory();

		$p = $pf->get_product( $product_id );

		$wrbp = new \WooCommerce_Role_Based_Price_Product_Pricing( false );

		return $wrbp->get_product_price( $p->get_sale_price(), $p, $price_key, $role_name );
	}
}


if ( ! function_exists( 'wasb_logger' ) ) {
	/**
	 * @param $callback
	 */
	function wasb_logger( $callback ) {
		$DESTINATION = 'wasb.log';

		if ( defined( 'WP_DEBUG_LOG' ) && WP_DEBUG_LOG ) {
			error_log( Carbon::now()->format( 'Y-m-d H:i:s' ) . ": START\n", 3, ABSPATH . $DESTINATION );

			if ( is_callable( $callback ) ) {
				$output = call_user_func( $callback );
			} else {
				$output = $callback;
			}

			error_log( "{$output}\n", 3, ABSPATH . $DESTINATION );

			error_log( Carbon::now()->format( 'Y-m-d H:i:s' ) . ": END\n", 3, ABSPATH . $DESTINATION );
		} else {
			if ( is_callable( $callback ) ) {
				call_user_func( $callback );
			}
		}
	}
}