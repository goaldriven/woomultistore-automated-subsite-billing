<?php defined( 'ABSPATH' ) or die( - 1 );

use Illuminate\Support\Carbon;

/**
 * 1) SCHEDULE EVENT WASB_AUTOBILLING_EVENT
 * 2) RESET SCHEDULE EVENT WASB_AUTOBILLING_EVENT UPON REQUEST
 */
add_action( 'admin_head', function () {
	// 1) or 2)
	if ( ! wp_next_scheduled( WASB_AUTOBILLING_EVENT, wasb_autobilling_event_args() )
	     || ( isset( $_POST[ WASB_AUTOBILLING_EVENT ] ) ) ) {
		wasb_autobilling_event_reschedule();
	}
} );


/**
 * CRON WASB_AUTOBILLING_EVENT PROCESS
 */
add_action( WASB_AUTOBILLING_EVENT, function ( $blog_id ) {
	switch_to_blog( intval( $blog_id ) );

	// only execute the cron if it registered under the blog
	if ( wp_next_scheduled( WASB_AUTOBILLING_EVENT, wasb_autobilling_event_args() ) ) {

		wasb_logger( function () {
			$blog_id = get_current_blog_id();

			$now = Carbon::now();

			// Create Billing (Quote)
			$post_id = wp_insert_post( [
				'post_title'    => sprintf( '%s %s Week %s Billing', $now->year, $now->monthName, $now->weekOfMonth ),
				'post_content'  => 'N/A',
				'post_type'     => 'sliced_quote',
				'post_status'   => 'publish',
				'post_date'     => date( 'Y-m-d H:i:s' ),
				'post_date_gmt' => get_gmt_from_date( date( 'Y-m-d H:i:s' ) )
			] );

			// Get Billing Client Id
			$client_id = get_blog_option( get_current_blog_id(), WASB_KIOSK_MANAGER_OPTION_NAME, false );;

			if ( ! is_wp_error( $post_id ) ) {

				// GET PRICING GROUP
				$pricing_role = wasb_get_current_role();

				// GET SUMMARY
				$date_from = wasb_default_date_range();
				$date_to   = wasb_default_date_range( 'to' );

				$sale_summary = wasb_site_sales_summary_by_date_range( $date_from, $date_to, $pricing_role );

				$items = $sale_summary->map( function ( $item ) {
					return [
						'qty'         => "{$item->sale_count}",
						'title'       => sprintf( '%s %s', get_the_title( $item->product_id ), $item->sku ),
						'tax'         => "0",
						'amount'      => "{$item->rowTotal}",
						'description' => "N/A",

						// will ignored in SlicedInvoice
						// only for auto stock transfer feature
						'product_SKU' => $item->sku,
						'product_ID'  => $item->product_id,
					];
				} );

				// GET SLICEINVOICE SETTINGS
				$sliced_quotes = get_blog_option( $blog_id, 'sliced_quotes', false );

				//GET QUOTE NUMBER & PREFIX
				$quote_number = "{$post_id}";

				if ( $next_quote_number = sliced_get_next_quote_number() ) {
					$quote_number = $next_quote_number;
				}

				update_post_meta( $post_id, '_sliced_quote_prefix', sliced_get_quote_prefix() );
				update_post_meta( $post_id, '_sliced_quote_number', $quote_number );
				update_post_meta( $post_id, '_sliced_description', '' );
				update_post_meta( $post_id, '_sliced_quote_terms', $sliced_quotes['terms'] );
				update_post_meta( $post_id, '_sliced_items', $items->toArray() );
				update_post_meta( $post_id, '_sliced_totals_for_ordering', wc_price( $items->sum( 'amount' ) ) );
				update_post_meta( $post_id, '_sliced_tax', '0.00' );
				update_post_meta( $post_id, '_sliced_tax_calc_method', 'exclusive' );
				update_post_meta( $post_id, '_sliced_currency_symbol', get_woocommerce_currency_symbol() );
				update_post_meta( $post_id, '_sliced_currency', get_woocommerce_currency() );
				update_post_meta( $post_id, '_sliced_client', $client_id );
				update_post_meta( $post_id, '_sliced_quote_created', date( 'U' ) );
				update_post_meta( $post_id, '_sliced_log', [
					date( 'U' ) => [
						'type' => 'quote_created',
						'by'   => "1"
					]
				] );

				// will ignored in SlicedInvoice
				// only for auto stock transfer feature
				do_action( WASB_TRACK_STOCK_TRANSFER_ACTION, $post_id, $items );
				//update_post_meta( $post_id, WASB_STOCK_TRANSFER_META, $items->toArray());

				Sliced_Quote::set_status( $post_id, 'sent' );

				return "Created quote with id {$post_id}";
			} else {
				return "Failed to create quote with id {$post_id}";
			}
		} );

	}
}, 10, 1 );
