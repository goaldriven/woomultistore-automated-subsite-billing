<?php defined('ABSPATH') or die(-1);

add_action('admin_head', function() {
	//PROCESS SELECTED PRICING ROLE
	if( $wasb_pricing_role = collect($_POST)->get(WASB_PRICING_ROLE_OPTION_NAME) ) {
		update_blog_option( get_current_blog_id(), WASB_PRICING_ROLE_OPTION_NAME, $wasb_pricing_role );
	}

	//PROCESS SELECTED KIOSK MANAGER
	if( $wasb_kiosk_manager = collect($_POST)->get(WASB_KIOSK_MANAGER_OPTION_NAME) ) {
		update_blog_option( get_current_blog_id(), WASB_KIOSK_MANAGER_OPTION_NAME, $wasb_kiosk_manager );

		// PROCESS CLIENT
		update_user_meta( $wasb_kiosk_manager, '_sliced_client_business', collect($_POST)->get('_sliced_client_business') );
		update_user_meta( $wasb_kiosk_manager, '_sliced_client_address', collect($_POST)->get('_sliced_client_address') );
		update_user_meta( $wasb_kiosk_manager, '_sliced_client_extra_info', collect($_POST)->get('_sliced_client_extra_info') );
	}

	//PROCESS SELECTED BILLING DATE RANGE
    /* DISABLED: it is unnessary for now
	$date_min_key = '_wasb_min_from_date';
	$date_max_key = '_wasb_max_from_date';

	if( $_POST ) {
		$generateBillValidator = (new JeffOchoa\ValidatorFactory())->make(
			$data = collect(array_merge($_POST,
				[
					$date_min_key => wasb_default_date_range(),
					$date_max_key => wasb_default_date_range('to')
				]))->only([WASB_ONDEMAND_FROM_DATE, WASB_ONDEMAND_TO_DATE, $date_min_key, $date_max_key])->toArray(),
			$rules = [
				$date_min_key => 'required',
				$date_max_key => 'required',
				WASB_ONDEMAND_FROM_DATE => 'required|date|after_or_equal:' . $date_min_key,
				WASB_ONDEMAND_TO_DATE   => 'required|date|after_or_equal:' . WASB_ONDEMAND_FROM_DATE . '|before_or_equal:' . $date_max_key
			]
		);

		if( $generateBillValidator->passes() ) {
			// generate bill
		} else if( $generateBillValidator->errors() ) {
			add_action( 'admin_notices', function() {
				?>
				<div class="notice notice-error is-dismissible">
				 <p><?= __( 'Make sure selected dates "Billing Date From" and "Billing Date From" are within this week.' ); ?></p>
				</div>
				<?php
			});
		}
	}
    */
});