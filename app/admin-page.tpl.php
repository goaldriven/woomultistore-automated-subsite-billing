<?php defined('ABSPATH') or die(-1);

/**
 * @var string $kiosk_manager_ID
 * @var string $formattedCronScheduled
 * @var string $sliced_client_extra_info
 * @var string $sliced_client_address
 * @var string $sliced_client_business
 * @var string $pricing_role
 * @var array $sliced_business
 * @var array $kiosk_managers
 * @var array $sale_summary
 * @var array $sliced_quote
 */
?>
	<style>
        .wasb input[type="date"],
        .wasb input[type="text"]{ width: 100%; margin-left: 0; padding: 6px; }
        .wasb input[type="submit"]{ float: right; }
        .wasb select{ width: 100%; max-height: 100%; }

		.woocommerce_page_autobilling-settings .postbox-container{
			float: unset;
		}

		.woocommerce_page_autobilling-settings #wpbody-content .metabox-holder{
			padding-top: unset;
		}

		.woocommerce_page_autobilling-settings .wrap .metabox-holder{
			margin-right: 12px;
		}

		.woocommerce_page_autobilling-settings .wrap{
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
		}

		.woocommerce_page_autobilling-settings .wrap .metabox-holder{
			width: 30%;
		}

		@media (min-width: 769px) and (max-width: 1025px) {
			.woocommerce_page_autobilling-settings .wrap .metabox-holder{
				width: 48%;
			}
		}

		@media (max-width: 768px) {
			.woocommerce_page_autobilling-settings .wrap .metabox-holder{
				width: 100%;
			}
		}
	</style>

	<h1><?= WASB_AUTOBILLING_PAGE_TITLE_TEXT ?></h1>

	<div class="wrap wasb">
        <!--BLOCK: SETTING-->
		<div class="metabox-holder">
			<div class="postbox-container">
				<div>
					<div class="postbox">
						<h2 class="hndle">
							<span><?= __('Settings') ?></span>
						</h2>
						<div class="inside">
							<div class="main">
								<form method="post">
									<table class="widefat">
										<?php if( is_array( $allowed_roles ) && !empty( $allowed_roles ) ): ?>
											<tr>
												<th class="row-title"><?= __('Pricing Group') ?></th>
												<th>
													<select name="<?= WASB_PRICING_ROLE_OPTION_NAME ?>"
													        id="<?= WASB_PRICING_ROLE_OPTION_NAME ?>"
													        style="width: 100%;"
													        required>
														<option value=""
															<?= !$kiosk_manager_ID ? 'selected="selected"' : '' ?>>
															<?= __('Select A Pricing Group') ?>
														</option>
														<?php foreach($allowed_roles as $role): ?>
															<option value="<?= $role ?>"
																<?= $pricing_role == $role ? 'selected="selected"' : '' ?>>
																<?= title_case( str_replace('_', ' ', $role ) ) ?>
															</option>
														<?php endforeach; ?>
													</select>
												</th>
											</tr>
											<tr>
												<th class="row-title" colspan="2">
													<hr>
													<strong><?= __('Kiosk Settings') ?></strong></th>
											</tr>
											<tr>
												<th class="row-title"><?= __('Kiosk Manager') ?></th>
												<th>
													<select name="<?= WASB_KIOSK_MANAGER_OPTION_NAME ?>"
													        id="<?= WASB_KIOSK_MANAGER_OPTION_NAME ?>"
													        style="width: 100%; max-height: 100%;"
													        required>
														<option value=""
															<?= !$kiosk_manager_ID ? 'selected="selected"' : '' ?>>
															<?= __('Select A Kiosk Manager') ?>
														</option>
														<?php foreach($kiosk_managers as $kiosk_manager): ?>
															<option value="<?= $kiosk_manager->ID ?>"
																<?= $kiosk_manager_ID == $kiosk_manager->ID ? 'selected="selected"' : '' ?>>
																<?= esc_html( $kiosk_manager->display_name ) . '(' . $kiosk_manager->user_email . ')' ?>
															</option>
														<?php endforeach; ?>
													</select>
												</th>
											</tr>
											<?php if( $kiosk_manager_ID ) : ?>
												<tr>
													<th class="row-title"><?= __('Name') ?></th>
													<th>
														<input type="text" name="_sliced_client_business"
														       value="<?= $sliced_client_business ?>">
													</th>
												</tr>
												<tr>
													<th class="row-title"><?= __('Address') ?></th>
													<th>
                                                            <textarea name="_sliced_client_address"
                                                                      style="width: 100%;max-width: 100%;padding: 6px;"><?= $sliced_client_address ?></textarea>
													</th>
												</tr>
												<tr>
													<th class="row-title"><?= __('Extra Info.') ?></th>
													<th>
														<input type="text" name="_sliced_client_extra_info"
														       value="<?= $sliced_client_extra_info ?>"
														       style="width: 100%;margin-left: 0;padding: 6px;">
													</th>
												</tr>
											<?php endif; ?>
											<tr>
												<th colspan="2">
													<input type="submit" class="button button-primary" value="Save">
												</th>
											</tr>
										<?php else: ?>
											<tr>
												<th>
													<p><?= sprintf( __('Please set <a href="%s"><strong>Allowed User Roles</strong></a>'),
															admin_url('admin.php?page=woocommerce-role-based-price-settings')) ?>
													</p>
												</th>
											</tr>
										<?php endif; ?>
									</table>
								</form>
								<br>
								<table class="widefat">
									<tr>
										<th class="row-title"><strong><?= __('Business Information') ?></strong></th>
										<th>
											<a href="<?= admin_url('admin.php?page=sliced_invoices_settings&tab=business') ?>"
											   class="button button-default"
											   style="float: right;"><?= __('Update') ?>
											</a>
										</th>
									</tr>
								</table>
								<br>
								<table class="widefat">
									<tr>
										<th class="row-title"><strong><?= __('Quote Settings') ?></strong></th>
										<th>
											<a href="<?= admin_url('admin.php?page=sliced_invoices_settings&tab=quotes') ?>"
											   class="button button-default"
											   style="float: right;"><?= __('Update') ?>
											</a>
										</th>
									</tr>
								</table>
								<br>
								<form method="post">
									<table class="widefat">
										<tr>
											<th class="row-title"><strong><?= __('Next Scheduled Autobilling') ?></strong><br/>
												<?= $formattedCronScheduled ?></th>
											<th>
												<input type="hidden" name="<?= WASB_AUTOBILLING_EVENT ?>" value="1">
												<input type="submit" class="button button-default" value="Reschedule">
											</th>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        <?php if( 1==0 ): // DISABLED: it is unnecessary for now ?>
        <!--BLOCK: GENERATE BILL-->
        <div class="metabox-holder">
            <div class="postbox-container">
                <div>
                    <div class="postbox">
                        <h2 class="hndle">
                            <span>Generate Bill</span>
                        </h2>
                        <div class="inside">
                            <div class="main">
                                <form method="post">
                                    <table class="widefat">
                                        <tr>
                                            <th class="row-title"><?= __('Billing Date From') ?></th>
                                            <th>
                                                <input type="date"
                                                       class="<?= WASB_ONDEMAND_FROM_DATE ?>"
                                                       name="<?= WASB_ONDEMAND_FROM_DATE ?>">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th class="row-title"><?= __('Billing Date To') ?></th>
                                            <th>
                                                <input type="date"
                                                       class="<?= WASB_ONDEMAND_TO_DATE ?>"
                                                       name="<?= WASB_ONDEMAND_TO_DATE ?>">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">
                                                <input type="submit" class="button button-default"  name="Generate">
                                            </th>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <!--BLOCK: PREVIEW-->
		<div class="metabox-holder">
			<div class="postbox-container">
				<div>
					<div class="postbox">
						<h2 class="hndle">
							<span>Preview (<?= wasb_default_date_range() ?> - <?= wasb_default_date_range('to') ?>)</span>
						</h2>
						<div class="inside">
							<div class="main">
								<table class="widefat">
									<?php if( $sliced_business ) : ?>
										<tr>
											<td colspan="4">
												<img src="<?= $sliced_business['logo'] ?>" alt="logo" width="60px">
												<br/>
												<br/>
												<p style="width: 70%;">
													<strong>From:</strong><br/>
													<?= $sliced_business['name'] ?><br/>
													<?= $sliced_business['address'] ?><br/>
													<?= $sliced_business['extra_info'] ?><br/>
													<?= $sliced_business['website'] ?>
												</p>
											</td>
										</tr>
									<?php endif; ?>
									<tr>
										<td colspan="4">
											<p style="width: 70%;">
												<strong>To:</strong><br/>
												<?= $sliced_client_business ?><br/>
												<?= $sliced_client_address ?><br/>
												<?= $sliced_client_extra_info ?>
											</p>
										</td>
									</tr>
									<tr>
										<th class="row-title">Product</th>
										<th>Qty</th>
										<th>Unit&nbsp;Price</th>
										<th>Sub&nbsp;Total</th>
									</tr>
									<?php foreach($sale_summary as $sale): ?>
										<tr>
											<td class="row-title"><?= get_the_title( $sale->product_id ) ?></td>
											<th><?= $sale->sale_count ?></th>
											<th><?= $sale->formattedUnitPrice ?></th>
											<th><?= $sale->formattedRowTotal ?></th>
										</tr>
									<?php endforeach; ?>
									<?php if( $sliced_quote ): ?>
										<tr>
											<td colspan="4" style="text-align: center;">
												<?= $sliced_quote['terms'] ?>
											</td>
										</tr>
										<tr>
											<td colspan="4" style="text-align: center;">
												<?= $sliced_quote['footer'] ?>
											</td>
										</tr>
									<?php endif; ?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php