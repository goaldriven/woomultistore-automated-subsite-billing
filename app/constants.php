<?php defined('ABSPATH') or die(-1);

define('WASB_AUTOBILLING_PAGE_TITLE_TEXT', __('Autobilling Panel'));
define('WASB_ONDEMAND_FROM_DATE', 'wasb_from_date');
define('WASB_ONDEMAND_TO_DATE', 'wasb_to_date');
define('WASB_PRICING_ROLE_OPTION_NAME', 'wasb_pricing_role');
define('WASB_KIOSK_MANAGER_OPTION_NAME', 'wasb_kiosk_manager');
define('WASB_AUTOBILLING_EVENT', 'wasb_cron_hook');
define('WASB_TRACK_STOCK_TRANSFER_ACTION', 'wasb_track_stock_transfer');
define('WASB_STOCK_TRANSFER_META', '_stock_transfer');
define('WASB_STOCK_TRANSFER_RECORDS_META', '_stock_transfer_records');
define('WASB_LAST_STOCK_TRANSFER_TIME_META', '_stock_transfer_time');

if( !defined('WASB_AUTOBILLING_EVENT_RECURRENCE') ) {
	define('WASB_AUTOBILLING_EVENT_RECURRENCE', 'daily');
}
