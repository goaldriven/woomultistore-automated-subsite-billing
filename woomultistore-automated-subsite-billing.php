<?php defined('ABSPATH') or die(-1);
/**
 * Plugin Name:     Woomultistore Automated Subsite Billing
 * Plugin URI:      https://goaldriven.co?s=Woomultistore+Automated+Subsite+Billing
 * Description:     bill subsite automation
 * Author:          GoalDriven
 * Author URI:      https://goaldriven.co
 * Text Domain:     woomultistore-automated-subsite-billing
 * Domain Path:     /languages
 * Version:         0.0.3
 *
 * @package         Woomultistore_Automated_Subsite_Billing
 */

try {
	require_once( __DIR__ . '/vendor/autoload.php' );
} catch ( Exception $e ) {
	return;
}

define('WASB_DIR', plugin_dir_path(__FILE__));
define('WASB_URL', plugin_dir_url(__FILE__));

require_once( WASB_DIR . 'app/constants.php' );
require_once( WASB_DIR . 'app/helpers.php' );
require_once( WASB_DIR . 'app/hook.php' );
include_once( WASB_DIR . 'app/admin-page.php' );
include_once( WASB_DIR . 'app/admin-page-save.php' );
include_once( WASB_DIR . 'app/cron.php' );
